﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bar
{
    public partial class Form1 : Form
    {
        private Copo meuCopo;
        public Form1()
        {
            InitializeComponent();
            meuCopo = new Copo();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido=("Cerveja");
            meuCopo.Capacidade = 100;
            trackBar1.Maximum = 100;

            label3.Text = meuCopo.ToString();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            meuCopo.Contem = trackBar1.Value;
            label2.Text = meuCopo.Contem.ToString();
            label3.Text = meuCopo.ToString();
            label4.Text= Convert.ToString(meuCopo.ValorEmPercentagem());

        }

        private void label3_Click(object sender, EventArgs e)
        {
            label3.Text= meuCopo.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            meuCopo.Esvaziar(100);
            label3.Text = meuCopo.ToString();
            label4.Text = Convert.ToString(meuCopo.ValorEmPercentagem());
            label2.Text = "0";
            trackBar1.Value = 0;
          

        }

        private void label2_Click(object sender, EventArgs e)
        {
          
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {    
            meuCopo.Liquido = ("Cocktail");
            meuCopo.Capacidade = 50;
            trackBar1.Maximum = 50;

            label3.Text = meuCopo.ToString();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = ("Sumo");
            meuCopo.Capacidade = 80;
            trackBar1.Maximum = 80;
            label3.Text = meuCopo.ToString();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = ("Água");
            meuCopo.Capacidade = 75;
            trackBar1.Maximum = 75;
            label3.Text = meuCopo.ToString();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
